docker ps -a  | grep Exited   | gawk '{print $1}' | xargs -L1 -n1 --no-run-if-empty docker rm

docker images | grep '<none>' | gawk '{print $3}' | xargs -L1 -n1 --no-run-if-empty docker rmi
