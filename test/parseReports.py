#!/usr/bin/env python

import os
import sys
import re
import json
import csv

from collections import OrderedDict

rec          = re.compile

parserReport = [
    ["date"                                         , rec("(\S+\s+\S+\s+\d+\s+\d+:\d+:\d+\s+\d+)")            , ""                                                       ], #- Mon Feb 27 12:23:29 2017
    ["projName"                                     , rec("\[(.+?)\]-\s+commit hash\s+=\s+\S+")               , ""                                                       ], #- [test01]- commit hash = b2fcf38
    ["hash"                                         , rec("\[.+?\]-\s+commit hash\s+=\s+(\S+)")               , ""                                                       ], #- [test01]- commit hash = b2fcf38
    ["checksum"                                     , rec("assembly checksum\s+=\s+(\S+)")                    , ""                                                       ], #- assembly checksum = 469,340,577,351,606
    ["reads"                                        , rec("([-+]?[0-9]*\.?[0-9]+\s+\S+)\s+=\s+READS")         , "number of reads; ideal 800-1200 for human"              ], #-    2.00 M   = READS          = number of reads; ideal 800-1200 for human
    ["mean_read_len"                                , rec("([-+]?[0-9]*\.?[0-9]+\s+\S+)\s+=\s+MEAN READ LEN") , "mean read length after trimming; ideal 140"             ], #-  139.00 b   = MEAN READ LEN  = mean read length after trimming; ideal 140
    ["effective_cov"                                , rec("([-+]?[0-9]*\.?[0-9]+\s+\S+)\s+=\s+EFFECTIVE COV") , "effective read coverage; ideal ~42 for nominal 56x cov" ], #-   53.26 x   = EFFECTIVE COV  = effective read coverage; ideal ~42 for nominal 56x cov
    ["read_two_q30"                                 , rec("([-+]?[0-9]*\.?[0-9]+\s+\S+)\s+=\s+READ TWO Q30")  , "fraction of Q30 bases in read 2; ideal 75-85"           ], #-   73.66 %   = READ TWO Q30   = fraction of Q30 bases in read 2; ideal 75-85
    ["median_insert"                                , rec("([-+]?[0-9]*\.?[0-9]+\s+\S+)\s+=\s+MEDIAN INSERT") , "median insert size; ideal 0.35-0.40"                    ], #-    0.34 kb  = MEDIAN INSERT  = median insert size; ideal 0.35-0.40
    ["proper_pairs"                                 , rec("([-+]?[0-9]*\.?[0-9]+\s+\S+)\s+=\s+PROPER PAIRS")  , "fraction of proper read pairs; ideal >=75"              ], #-  100.00 %   = PROPER PAIRS   = fraction of proper read pairs; ideal >=75
    ["molecule_len"                                 , rec("([-+]?[0-9]*\.?[0-9]+\s+\S+)\s+=\s+MOLECULE LEN")  , "weighted mean molecule size; ideal 50-100"              ], #-   67.60 kb  = MOLECULE LEN   = weighted mean molecule size; ideal 50-100
    ["het_dist"                                     , rec("([-+]?[0-9]*\.?[0-9]+\s+\S+)\s+=\s+HETDIST")       , "mean distance between heterozygous SNPs"                ], #-    2.00 kb  = HETDIST        = mean distance between heterozygous SNPs
    ["unbar"                                        , rec("([-+]?[0-9]*\.?[0-9]+\s+\S+)\s+=\s+UNBAR")         , "fraction of reads that are not barcoded"                ], #-    0.00 %   = UNBAR          = fraction of reads that are not barcoded
    ["barcode_n50"                                  , rec("([-+]?[0-9]*\.?[0-9]+)\s+=\s+BARCODE N50")         , "N50 reads per barcode"                                  ], #- 2160.00     = BARCODE N50    = N50 reads per barcode
    ["dups"                                         , rec("([-+]?[0-9]*\.?[0-9]+\s+\S+)\s+=\s+DUPS")          , "fraction of reads that are duplicates"                  ], #-    0.12 %   = DUPS           = fraction of reads that are duplicates
    ["phased"                                       , rec("([-+]?[0-9]*\.?[0-9]+\s+\S+)\s+=\s+PHASED")        , "nonduplicate and phased reads; ideal 45-50"             ], #-   83.93 %   = PHASED         = nonduplicate and phased reads; ideal 45-50
    ["long_scaffolds"                               , rec("([-+]?[0-9]*\.?[0-9]+\s+\S+)\s+=\s+LONG SCAFFOLDS"), "number of scaffolds >= 10 kb"                           ], #-    0.02 K   = LONG SCAFFOLDS = number of scaffolds >= 10 kb
    ["edge_n50"                                     , rec("([-+]?[0-9]*\.?[0-9]+\s+\S+)\s+=\s+EDGE N50")      , "N50 edge size"                                          ], #-    4.97 kb  = EDGE N50       = N50 edge size
    ["contig_n50"                                   , rec("([-+]?[0-9]*\.?[0-9]+\s+\S+)\s+=\s+CONTIG N50")    , "N50 contig size"                                        ], #-    6.96 kb  = CONTIG N50     = N50 contig size
    ["phaseblock_n50"                               , rec("([-+]?[0-9]*\.?[0-9]+\s+\S+)\s+=\s+PHASEBLOCK N50"), "N50 phase block size"                                   ], #-    1.07 Mb  = PHASEBLOCK N50 = N50 phase block size
    ["scaffold_n50"                                 , rec("([-+]?[0-9]*\.?[0-9]+\s+\S+)\s+=\s+SCAFFOLD N50")  , "N50 scaffold size"                                      ], #-    1.05 Mb  = SCAFFOLD N50   = N50 scaffold size
    ["scaffold_n60"                                 , rec("([-+]?[0-9]*\.?[0-9]+\s+\S+)\s+=\s+SCAFFOLD N60")  , "N60 scaffold size"                                      ], #-    1.01 Mb  = SCAFFOLD N60   = N60 scaffold size
    ["assembly_size"                                , rec("([-+]?[0-9]*\.?[0-9]+\s+\S+)\s+=\s+ASSEMBLY SIZE") , "assembly size (only scaffolds >= 10 kb)"                ], #-    0.00 Gb  = ASSEMBLY SIZE  = assembly size (only scaffolds >= 10 kb)
]


parserConfig = [
    ["haplotypes_to_simulate"                       , rec("-d\s+(\d+)")                                       , "Haplotypes to simulate [2]"                             ], #-d INT      Haplotypes to simulate [2]
    ["haploid_fastas"                               , rec("-g\s+(\S+)")                                       , "Haploid FASTAs separated by comma. Overrides -r and -d."], #-g STRING   Haploid FASTAs separated by comma. Overrides -r and -d.
    ["1_SNP_per_base_pairs"                         , rec("-1\s+(\d+)")                                       , "1 SNP per INT base pairs [1000]"                        ], #-1 INT      1 SNP per INT base pairs [1000]
    ["minimum_len_of_indels"                        , rec("-2\s+(\d+)")                                       , "Minimum length of Indels  [1]"                          ], #-2 INT      Minimum length of Indels  [1]
    ["maximum_len_of_indels"                        , rec("-3\s+(\d+)")                                       , "Maximum length of Indels  [50]"                         ], #-3 INT      Maximum length of Indels  [50]
    ["num_indels"                                   , rec("-4\s+(\d+)")                                       , "# of Indels  [1000]"                                    ], #-4 INT      # of Indels  [1000]
    ["min_len_of_dup_inv"                           , rec("-5\s+(\d+)")                                       , "Minimum length of Duplications and Inversions [1000]"   ], #-5 INT      Minimum length of Duplications and Inversions [1000]
    ["max_len_of_dup_inv"                           , rec("-6\s+(\d+)")                                       , "Maximum length of Duplications and Inversions [10000]"  ], #-6 INT      Maximum length of Duplications and Inversions [10000]
    ["num_dup_inv"                                  , rec("-7\s+(\d+)")                                       , "# of Duplications and # of Inversions [100]"            ], #-7 INT      # of Duplications and # of Inversions [100]
    ["min_len_transloc"                             , rec("-8\s+(\d+)")                                       , "Minimum length of Translocations [1000]"                ], #-8 INT      Minimum length of Translocations [1000]
    ["max_len_transloc"                             , rec("-9\s+(\d+)")                                       , "Maximum length of Translocations [10000]"               ], #-9 INT      Maximum length of Translocations [10000]
    ["num_transloc"                                 , rec("-0\s+(\d+)")                                       , "# of Translocations [100]"                              ], #-0 INT      # of Translocations [100]
    ["per_base_error_rate_of_the_first_read"        , rec("-e\s+([-+]?[0-9]*\.?[0-9]+)")                      , "Per base error rate of the first read [0.0001,0.0016]"  ], #-e FLOAT    Per base error rate of the first read [0.0001,0.0016]
    ["per_base_error_rate_of_the_second_read"       , rec("-E\s+([-+]?[0-9]*\.?[0-9]+)")                      , "Per base error rate of the second read [0.0001,0.0016]" ], #-E FLOAT    Per base error rate of the second read [0.0001,0.0016]
    ["outer_distance_between_the_two_ends_for_pairs", rec("-i\s+(\d+)")                                       , "Outer distance between the two ends for pairs [350]"    ], #-i INT      Outer distance between the two ends for pairs [350]
    ["standard_deviation_of_the_distance_for_pairs" , rec("-s\s+(\d+)")                                       , "Standard deviation of the distance for pairs [35]"      ], #-s INT      Standard deviation of the distance for pairs [35]
    ["read_length"                                  , rec("-l\s+(\d+)")                                       , "Read length (with barcode of 16 bp) [151]"              ], #-l INT      Read length (with barcode of 16 bp) [151]
    ["barcodes_list"                                , rec("-b\s+(\S+)")                                       , "Barcodes list"                                          ], #-b STRING   Barcodes list
    ["million_reads_pairs_in_total_to_simulated"    , rec("-x\s+(\d+)")                                       , "# million reads pairs in total to simulated [600]"      ], #-x INT      # million reads pairs in total to simulated [600]
    ["Mean_molecule_length_in_kbp"                  , rec("-f\s+(\d+)")                                       , "Mean molecule length in kbp [100]"                      ], #-f INT      Mean molecule length in kbp [100]
    ["input_list_of_fragment_sizes"                 , rec("-c\s+(\S+)")                                       , "Input a list of fragment sizes"                         ], #-c STRING   Input a list of fragment sizes
    ["nx1000_partitions_to_generate"                , rec("-t\s+(\d+)")                                       , "n*1000 partitions to generate [1500]"                   ], #-t INT      n*1000 partitions to generate [1500]
    ["Average_num_of_molecules_per_partition"       , rec("-m\s+(\d+)")                                       , "Average # of molecules per partition [10]"              ], #-m INT      Average # of molecules per partition [10]
    ["Continue_from_a_step"                         , rec("-u\s+(\d+)")                                       , "Continue from a step [auto]"                            ], #-u INT      Continue from a step [auto]
                                                                                                                                                                                   #  1. Variant simulation
                                                                                                                                                                                   #  2. Build fasta index
                                                                                                                                                                                   #  3. DWGSIM
                                                                                                                                                                                   #  4. Simulate reads
                                                                                                                                                                                   #  5. Sort reads extraction manifest
                                                                                                                                                                                   #  6. Extract reads
    ["num_of_threads_to_run_DWGSIM"                 , rec("-z\s+(\d+)")                                       , "# of threads to run DWGSIM [8]"                         ], #-z INT      # of threads to run DWGSIM [8]
    ["disable_parameter_checking"                   , rec("(-o)")                                             , "Disable parameter checking"                             ], #-o          Disable parameter checking
]



def parseFileRe(infile, rexp, name=None):
    res = OrderedDict()

    with open(infile, 'r') as fhd:
        for line in fhd:
            # print line
            for pk, reg, desc in rexp:
                m = reg.search(line)
                
                if m:
                    res[pk] = m.group(1)

    for pk, reg, desc in rexp:
        if pk not in res:
            print "{}variable {} not found".format('' if name is None else '{} :: '.format(name), pk)

    print " VARIABLES FOUND {}".format(len(res))

    return res


def parseSummaryFile(infile):
    """
    cat outs/summary.csv
    longranger_version,instrument_ids,gems_detected,mean_dna_per_gem,bc_on_whitelist,bc_mean_qscore,n50_linked_reads_per_molecule,corrected_loaded_mass_ng,snps_phased,genes_phased_lt_100kb,longest_phase_block,n50_phase_block,molecule_length_mean,molecule_length_stddev,number_reads,median_insert_size,mean_depth,zero_coverage,mapped_reads,pcr_duplication,on_target_bases,r1_q20_bases_fract,r1_q30_bases_fract,r2_q20_bases_fract,r2_q30_bases_fract,si_q20_bases_fract,si_q30_bases_fract,bc_q20_bases_fract,bc_q30_bases_fract,large_sv_calls,short_deletion_calls
    2.1.3,AE005174-1_31025_31199_0_1_0_0_0;AE005174v2-2_1378458_1378711_0_1_0_0_0;AE005174v2-2_3549312_3549154_1_0_0_0_0;AE005174-1_14515_14253_1_0_0_0_0;AE005174-1_19463_19625_0_1_0_0_0;AE005174-1_22744_22905_0_1_0_0_0;AE005174v2-2_3546864_3546639_1_0_0_0_0;AE005174v2-2_1378984_1378783_1_0_0_0_0;AE005174-1_19110_18883_1_0_0_0_0;AE005174-1_9049_8925_1_0_0_0_0,984,1245740.4115757935,0.9999930118588755,39.17604849570301,107,2.9304490695524588,0.997907949790795,,3794843,3794843,90997.77792981449,69662.68693171069,2003394,344,50.50564299749511,0.0067109342051390975,1.0,1.4062772973328989e-05,,0.9999617732042058,0.6324404984740895,0.9999984529564587,0.6069670365416379,NaN,NaN,0.9996692487848122,0.9996692487848122,8,7
    """
    
    """
    $ cat outs/summary.csv
    assembly_size,bases_per_read,checksum,contig_N50,dup_perc,edge_N50,effective_coverage,hetdist,lw_mean_mol_len,median_ins_sz,nreads,phase_block_N50,placed_frac,proper_pairs_perc,q30_r2_perc,rpb_N50,sample_id,scaffold_N50,scaffolds_10kb_plus,scaffolds_1kb_plus,valid_bc_perc
    4281034,139,469340577351606,6961,0.124289,4969,53.2576,1997,67602,344,2003394,1067068,0.839267,99.9997,73.6588,2160,test01,1049875,16,300,99.9993
    """
    data = []
    with open(infile, 'r') as fhd:
        for ln, line in enumerate(fhd):
            line = line.strip()
            
            if len(line) == 0:
                continue
            
            if ln == 0:
                data = [[c,None] for c in line.split(',')]
                
            else:
                cols = line.split(',')
                
                for b,c in enumerate(cols):
                    if '.' in c:
                        try:
                            c = float(c)
                        except:
                            pass
                    else:
                        try:
                            c = int(c)
                        except:
                            pass
                    data[b][1] = c
    
    return OrderedDict(data)

  
def parseReportFile(infile):
    """
    $ cat outs/report.txt
    --------------------------------------------------------------------------------
    SUMMARY
    --------------------------------------------------------------------------------
    - Mon Feb 27 12:23:29 2017
    - [test01]- commit hash = b2fcf38
    - assembly checksum = 469,340,577,351,606
    --------------------------------------------------------------------------------
    INPUT
    -    2.00 M   = READS          = number of reads; ideal 800-1200 for human
    -  139.00 b   = MEAN READ LEN  = mean read length after trimming; ideal 140
    -   53.26 x   = EFFECTIVE COV  = effective read coverage; ideal ~42 for nominal 56x cov
    -   73.66 %   = READ TWO Q30   = fraction of Q30 bases in read 2; ideal 75-85
    -    0.34 kb  = MEDIAN INSERT  = median insert size; ideal 0.35-0.40
    -  100.00 %   = PROPER PAIRS   = fraction of proper read pairs; ideal >=75
    -   67.60 kb  = MOLECULE LEN   = weighted mean molecule size; ideal 50-100
    -    2.00 kb  = HETDIST        = mean distance between heterozygous SNPs
    -    0.00 %   = UNBAR          = fraction of reads that are not barcoded
    - 2160.00     = BARCODE N50    = N50 reads per barcode
    -    0.12 %   = DUPS           = fraction of reads that are duplicates
    -   83.93 %   = PHASED         = nonduplicate and phased reads; ideal 45-50
    --------------------------------------------------------------------------------
    OUTPUT
    -    0.02 K   = LONG SCAFFOLDS = number of scaffolds >= 10 kb
    -    4.97 kb  = EDGE N50       = N50 edge size
    -    6.96 kb  = CONTIG N50     = N50 contig size
    -    1.07 Mb  = PHASEBLOCK N50 = N50 phase block size
    -    1.05 Mb  = SCAFFOLD N50   = N50 scaffold size
    -    1.01 Mb  = SCAFFOLD N60   = N60 scaffold size
    -    0.00 Gb  = ASSEMBLY SIZE  = assembly size (only scaffolds >= 10 kb)
    --------------------------------------------------------------------------------
    """
    
    rep = parseFileRe(infile, parserReport, name='supernova report :: {}'.format(infile))
    
    return rep


def parseConfigFile(configFile):
    cfg = parseFileRe(configFile, parserConfig, name='config :: {}'.format(configFile))

    return OrderedDict({'config': cfg })


def parseLongranger(infolder):
    outFolder = os.path.join(infolder, 'outs')
    assert os.path.exists(outFolder)
    assert os.path.isdir( outFolder)

    summaryFile = os.path.join(outFolder, 'summary.csv')
    
    print "   longranger summary file: {}".format(summaryFile)

    assert os.path.exists(summaryFile)
    assert os.path.isfile(summaryFile)
    
    summary = parseSummaryFile(summaryFile)
    
    return OrderedDict((('summary', summary),))


def parseSupernova(infolder):
    outFolder = os.path.join(infolder, 'outs')

    print "  supernova output folder: {}".format(outFolder)
    
    assert os.path.exists(outFolder)
    assert os.path.isdir( outFolder)
    
    reportFile  = os.path.join(outFolder, 'report.txt' )
    summaryFile = os.path.join(outFolder, 'summary.csv')
    
    print "   supernova report  file: {}".format(reportFile )
    print "   supernova summary file: {}".format(summaryFile)

    assert os.path.exists(reportFile)
    assert os.path.isfile(reportFile)
    
    assert os.path.exists(summaryFile)
    assert os.path.isfile(summaryFile)
    
    report  = parseReportFile( reportFile )
    summary = parseSummaryFile(summaryFile)
    
    return OrderedDict((('report', report),('summary', summary)))


def processProjects(projectnames, outfile=None, do_json=True, do_csv=True, verbose=False):
    data = OrderedDict()
    for projectname in projectnames:
        print "parsing project: {}".format(projectname)
        
        assert os.path.exists(projectname)
        assert os.path.isdir( projectname)
        
        configFile      =              projectname + ".cfg"
        mapFolder       = os.path.join(projectname, 'map_'       + projectname)
        denovoFolder    = os.path.join(projectname, 'denovo_'    + projectname)
        denovoMapFolder = os.path.join(projectname, 'denovomap_' + projectname)
        
        print " config file     : {}".format(configFile     )
        print " map       folder: {}".format(mapFolder      )
        print " denovo    folder: {}".format(denovoFolder   )
        print " denovomap folder: {}".format(denovoMapFolder)
        
        assert os.path.exists(configFile)
        assert os.path.isfile(configFile)
        
        # assert os.path.exists(mapFolder)
        # assert os.path.isdir( mapFolder)
        # 
        # assert os.path.exists(denovoFolder)
        # assert os.path.isdir( denovoFolder)
        # 
        # assert os.path.exists(denovoMapFolder)
        # assert os.path.isdir( denovoMapFolder)
        
        data[projectname]              = OrderedDict()
        data[projectname]['config'   ] = parseConfigFile( configFile      )
        if os.path.exists(mapFolder      ) and os.path.isdir(mapFolder   ): data[projectname]['map'      ] = parseLongranger( mapFolder       )
        if os.path.exists(denovoFolder   ) and os.path.isdir(denovoFolder): data[projectname]['denovo'   ] = parseSupernova ( denovoFolder    )
        if os.path.exists(denovoMapFolder) and os.path.isdir(mapFolder   ): data[projectname]['denovomap'] = parseLongranger( denovoMapFolder )

    
    if outfile is not None:
        if do_csv:
            print "saving to {}".format(outfile + '.csv')

            with open(outfile + '.csv', 'wb') as csvfile:
                keys = OrderedDict()
                for projnum, (projectname, projectinfo) in enumerate(data.iteritems()):
                    for runnum, (runtype, runinfo) in enumerate(projectinfo.iteritems()):
                        for filenum, (filetype, fileinfo) in enumerate(runinfo.iteritems()):
                            for colnum, (colname, colval) in enumerate(fileinfo.iteritems()):
                                k = '/'.join( [runtype, filetype, colname] )
                                if runtype not in keys:
                                    keys[runtype] = OrderedDict()
                                if filetype not in keys[runtype]:
                                    keys[runtype][filetype] = OrderedDict()
                                if k not in keys[runtype][filetype]:
                                    keys[runtype][filetype][colname] = 0
                                keys[runtype][filetype][colname] += 1

                csvwriter = csv.writer(csvfile, delimiter='\t', quotechar='"', quoting=csv.QUOTE_MINIMAL)
                colNames = ['proj_num', 'projectname']
                
                for runnum, (runtype, filetypes) in enumerate(keys.iteritems()):
                    for filenum, (filetype, colnames) in enumerate(filetypes.iteritems()):
                        for colnum, (colname, colcount) in enumerate(colnames.iteritems()):
                            colNames.append( '/'.join( [runtype, filetype, colname] ) )

                csvwriter.writerow(colNames)

                for projnum, (projectname, projectinfo) in enumerate(data.iteritems()):
                    if verbose: print " PROJECT", projnum, projectname
                    
                    cols = []
                    
                    for runnum, (runtype, filetypes) in enumerate(keys.iteritems()):
                        if verbose: print "  RUN", runnum, runtype
                        runinfo = projectinfo.get(runtype, None)
                        
                        if runinfo is None:
                            cols.extend(list("-"*sum([len(cc) for cn, cc in filetypes.iteritems()])))
                            continue
                        
                        for filenum, (filetype, colnames) in enumerate(filetypes.iteritems()):
                            if verbose: print "   FILE", filenum, filetype
                            fileinfo = runinfo.get(filetype, None)
                            
                            if runinfo is None:
                                cols.extend(list("+"*len(colnames)))
                                continue
                            
                            for colnum, (colname, colcount) in enumerate(colnames.iteritems()):
                                if verbose: print "    COL", colnum, colname, colval
                                colval = fileinfo.get(colname, "*")
                                cols.append(colval)
                        
                                # for runnum, (runtype, runinfo) in enumerate(projectinfo.iteritems()):
                                # for filenum, (filetype, fileinfo) in enumerate(runinfo.iteritems()):
                                # for colnum, (colname, colval) in enumerate(fileinfo.iteritems()):

                    csvwriter.writerow( [projnum+1, projectname] + cols)
                    
                csvwriter.writerow( [])
                csvwriter.writerow( ['##', 'COLUMN DESCRIPTIONS'])
                for rnam, rexp in [['report', parserReport], [ 'config', parserConfig ]]:
                    for pk, reg, desc in rexp:
                        csvwriter.writerow( ['#', rnam, pk, desc])
    
        if do_json:
            print "saving to {}".format(outfile + '.json')
            data['_desc'] = OrderedDict()
            
            for rnam, rexp in [['report', parserReport], [ 'config', parserConfig ]]:
                data['_desc'][rnam] = OrderedDict()
                for pk, reg, desc in rexp:
                    data['_desc'][rnam][pk] = desc

            if verbose:
                print json.dumps(data, indent=1, sort_keys=True)
            
            json.dump(data, open(outfile + '.json', 'w'), indent=1, sort_keys=True)
    
    return data


def main():
    outfile      = sys.argv[1]
    projectnames = sys.argv[2:]
    
    if os.path.exists(outfile + '.json'):
        print "Output file {} already exists. not overwritting".format( outfile + '.json' )
        sys.exit(1)
    
    if os.path.exists(outfile + '.csv'):
        print "Output file {} already exists. not overwritting".format( outfile + '.csv' )
        sys.exit(1)
        
    processProjects(projectnames, outfile=outfile, do_json=True, do_csv=True, verbose=True)

if __name__ == '__main__':
    main()
