#!/usr/bin/env python

# coding: utf-8

# Create 500 sequences, by combining the shortest ones into a single big one.
# Separate by 100Ns

# In[17]:

import os
import sys
import gzip
from os.path       import basename
from Bio           import SeqIO
from Bio.Seq       import Seq
from Bio.SeqRecord import SeqRecord


number_of_contigs = 499
minimal_size      = 200
Ns                = "N" * 1001

def getFileHandler(filename, mode):
    if filename.endswith('.gz'):
        return gzip.open(filename, mode+'b')
    else:
        return open(filename, mode)

def process(filename):
    print "reading {}".format(filename)
    
    skipped      = 0
    concatenated = 0
    
    with getFileHandler(filename, 'r') as handle:
        l = SeqIO.parse(handle, "fasta")
        
        sortedList       = [f for f in sorted(l, key=lambda x : len(x.seq), reverse=True)]
        concatenate_list = []
        
        print "found {} sequences".format(len(sortedList))
        
        for idx, val in enumerate(sortedList):
            # print " processing #{}: {}".format(idx, val)
            if idx < number_of_contigs:
                if len(sortedList[idx].seq) >= minimal_size:
                    pass
                else:
                    skipped += 1
                    
                # sortedList[idx]=''
                
            else:
                if len(sortedList[idx].seq) >= minimal_size:
                    concatenate_list.append(sortedList[idx])
                    concatenated += 1
                else:
                    skipped += 1
                    
                sortedList[idx]=None

        print "Skipped      {:12,d} contigs that were shorter than {:12,d}".format(skipped     , minimal_size     )
        print "Concatenated {:12,d} contigs after {:12,d} contigs"         .format(concatenated, number_of_contigs)
    
    ext                 = '.gz' if filename.endswith('.gz') else ''
    largest_file        = filename + '.concat.largest.fasta'    + ext
    not_largest_file    = filename + '.concat.notlargest.fasta' + ext
    all_file            = filename + '.concat.all.fasta'        + ext

    if skipped == 0 and concatenated == 0:
        print "no concatenation or skip. creating hardlink"
        
        if os.path.exists(all_file):
            os.remove(all_file)
            
        os.link(filename, all_file)
        
    else:
        sortedList          = filter(None, sortedList)
            
        conseq              = Seq(Ns.join([str(seq_rec.seq)      for seq_rec in concatenate_list]))
        conseq_descriptions = '|'.join([str(seq_rec.description) for seq_rec in concatenate_list])
        
        conseq_r            = SeqRecord(conseq)
        
        conseq_r.id         = conseq_descriptions
        
        with getFileHandler(largest_file, 'w') as fhd:
            # outfile = basename(filename) + '.largest.' + str(number_of_contigs) + '.' + str(minimal_size) + '.fasta'
            
            print "Saving largest to {}".format(largest_file)
            
            SeqIO.write(sortedList, fhd, "fasta")
        
        
        with getFileHandler(not_largest_file, 'w') as fhd:
            # outfile = basename(filename) + '.notlargest.' + str(number_of_contigs) + '.' + str(minimal_size) + '.concatenated.fasta'
            
            print "Saving not largest to {}".format(largest_file)
        
            SeqIO.write(conseq_r, fhd, "fasta")
    
    
        with getFileHandler(all_file, 'w') as fhd:
            print "Saving all to {}".format(all_file)
            
            SeqIO.write(sortedList, fhd, "fasta")
            SeqIO.write(conseq_r  , fhd, "fasta")

def main():
    process(sys.argv[1])
    
if __name__ == '__main__':
    main()


